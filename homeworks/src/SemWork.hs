-- Структура данных. Может быть переменной, лямбдой от переменной, возвращающей терм или выполнением терма 
data Term = Variable String
          | Lambda String Term
          | Application Term Term


-- Класса Show для типа вывода Term
instance Show Term where
    show (Variable x) = x
    show (Application term1 term2) = (show term1) ++ " " ++ (show term2)
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")" 

-- получаем уникальную строку, которой нет в списке
-- проверяем есть ли переменная в списке,если да, то новая переменная , иначе оставляем
renameVariable :: [String] -> String -> Int -> String
renameVariable lst x n | elem (x ++ (show n)) lst = renameVariable lst x (n+1) 
                       | otherwise = x ++ (show n)

-- совпадающие переменные с теми, что есть в списке переименовываем
-- проверяем есть ли у нас String в списке переменных, то  удаляем из списка эту переменную
-- если нет то оставляем 
renameFreeVariables :: [String] -> Term -> Term
renameFreeVariables lst (Lambda x term) | elem x lst = Lambda x (renameFreeVariables (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVariables lst term)
renameFreeVariables lst (Application t1 t2) = Application (renameFreeVariables lst t1) (renameFreeVariables lst t2)
renameFreeVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                                     | otherwise = Variable x


-- функция для замены данной переменной на нужный терм
-- применение reduce, сама замена 
setVariable :: String -> [String] -> Term -> Term -> Term

setVariable var lst (Lambda x term) needTerm | x /= var = Lambda x (setVariable var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVariable var lst (Application term1 term2) needTerm = Application (setVariable var lst term1 needTerm) (setVariable var lst term2 needTerm)
setVariable var lst (Variable x) needTerm | x == var = renameFreeVariables lst needTerm
                                          | otherwise = Variable x  

-- один шаг в нормализации терма
eval1 :: Term -> Term
eval1 (Application (Lambda var res) t2) = setVariable var [] res t2
eval1 (Application (Application term1 term2) t2) = Application (eval1 (Application term1 term2)) t2
eval1 term = term


-- максимальное число шагов в нормализации терма
eval :: Term -> Term
eval (Application (Lambda var res) t2) = eval (setVariable var [] res t2)
eval (Application (Application term1 term2) t2) = eval (Application (eval (Application term1 term2)) t2)
eval term = term

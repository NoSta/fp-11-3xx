
import Test.Tasty (TestTree(..), defaultMainWithIngredients, testGroup)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

import Test.Tasty (TestTree(..), testGroup, Timeout(..), localOption)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Control.Arrow
import Data.Function
import Data.List

import Trouble
  
main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [ testGroup "Rocket tests" rocketTests
        , testGroup "Orbiters tests" orbitersTests
        , testGroup "Final tests" finalTests
        ]

rocketTests :: [TestTree]
rocketTests =
  [ testCase "Example rocket 1" $ rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)] @?= [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
  , testCase "Example rocket 2" $ rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)] @?= [(120,2)]
  , testProperty "Top stage" $ \ys -> length ys >= 1 && all ((/=0).snd) ys ==> let
       xs = map (second abs) ys
       ans = rocket xs
       d (a,b) = a`div`b
       in case ans of
         [(a,b)] -> all ((<= d (a,b)) . d) xs && snd (minimumBy (compare`on`snd) xs) == b
         _ -> snd (maximumBy (compare`on`d) xs) > snd (minimumBy (compare`on`snd) xs)
  ]

orbitersTests :: [TestTree]
orbitersTests =
  [ testCase "Example orbiters" $ orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]] @?= [Orbiter "ISS", Orbiter "LRO"]
  , testProperty "Only orbiters" $ \rocket -> all isOrbiter $ orbiters (rocket :: [Spaceship Int])
  ]
  where isOrbiter (Orbiter _) = True
        isOrbiter _ = False

instance Arbitrary a => Arbitrary (Load a) where
  arbitrary = oneof [Orbiter <$> arbitrary, Probe <$> arbitrary]

instance Arbitrary a => Arbitrary (Spaceship a) where
  arbitrary = sized spaceship

spaceship n = if n <= 0
              then Cargo <$> arbitrary
              else do
                k <- arbitrary `suchThat` (>0)
                Rocket (k*100) <$> mapM spaceship [n-k..n-1]

instance Eq Phrase where
  (==) = undefined

newtype Trekkie = Trekkie { unTrekkie :: (Phrase, String) } deriving (Show)

finalTests :: [TestTree]
finalTests =
  [ testCase "Example trek" $ finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper] @?= ["McCoy", "Spock", "Kirk", "Spock"]
  , testProperty "Random trek" $ \xs -> finalFrontier (map (fst . unTrekkie) xs) === map (snd . unTrekkie) xs
  ]

instance Arbitrary Trekkie where
  arbitrary = Trekkie <$> oneof
              [ arbitrary >>= \x -> return (Warp x, "Kirk")
              , arbitrary >>= \x -> return (BeamUp x, "Kirk")
              , arbitrary >>= \x -> return (IsDead x, "McCoy")
              , return (LiveLongAndProsper, "Spock")
              , return (Fascinating, "Spock") ]

